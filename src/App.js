import React from "react";
import { connect } from "react-redux";
import { BrowserRouter } from "react-router-dom";

import GlobalStyle from "@/styles/global";

import { Template } from "@/styles/template/mainTemplate";
import Header from "@/components/partials/Header";
//import Footer from "@/components/partials/Footer";

import Routes from "@/Routes";

function App(props) {
  return (
    <BrowserRouter>
      <GlobalStyle />
      <Template>
        <Header />
        <Routes />
      </Template>
    </BrowserRouter>
  );
}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

import React from "react";
import { Link } from "react-router-dom";
import { HeaderArea } from "./styles";

import logo from "@/assets/logo.jpg";

import { isLoggad } from "@/helpers/AuthHandler";

const Header = () => {
  let logged = isLoggad();

  return (
    <HeaderArea>
      <div className="container">
        <div className="logo">
          <Link to="/">
            <img className="logo-img" src={logo} alt="Logo" />
          </Link>
        </div>
        <nav>
          <ul>
            {logged && (
              <>
                <li>
                  <Link to="/my-account">Minha Conta</Link>
                </li>
                <li>
                  <Link to="/logout">Sair</Link>
                </li>
                <li>
                  <Link className="button" to="/post-an-ad">
                    Criar um Anúncio
                  </Link>
                </li>
              </>
            )}
            {!logged && (
              <>
                <li>
                  <Link to="/signin">Login</Link>
                </li>
                <li>
                  <Link to="/signup">Cadastrar</Link>
                </li>
                <li>
                  <Link className="button" to="/signin">
                    Criar um Anúncio
                  </Link>
                </li>
              </>
            )}
          </ul>
        </nav>
      </div>
    </HeaderArea>
  );
};

export default Header;

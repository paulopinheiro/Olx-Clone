import styled from "styled-components";

export const HeaderArea = styled.div`
  background-color: #fff;
  height: 80px;
  border-bottom: 1px solid #ccc;

  .container {
    max-width: 1000px;
    margin: auto;
    display: flex;
  }

  a {
    text-decoration: none;
  }

  .logo {
    flex: 1;
    display: flex;
    align-items: center;
    height: 80px;

    .logo-img {
      height: 60px;
      width: 60px;
      border-radius: 50%;
    }
  }

  nav {
    padding-top: 10px;
    padding-bottom: 10px;
  }
  ul {
    display: flex;
    align-items: center;
    height: 60px;
  }
  li {
    margin-left: 20px;
    margin-right: 20px;

    a {
      color: #000;
      font-size: 16px;

      &:hover {
        color: #999;
      }

      &.button {
        background-color: rgb(247, 131, 35);
        color: rgb(255, 255, 255);
        border-radius: 5px;
        padding: 10px 10px;
      }
      &.button:hover {
        background-color: rgba(247, 131, 35, 0.8);
      }
    }
  }
`;

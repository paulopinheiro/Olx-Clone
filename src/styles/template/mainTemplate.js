import styled from "styled-components";

export const Template = styled.div``;

export const PageContainer = styled.div`
  max-width: 1000px;
  margin: auto;
`;

export const PageTitle = styled.h1`
  padding: 15px;
  font-size: 30px;
`;

export const PageBody = styled.div``;

export const ErrorMessage = styled.div`
  font-size: 20px;
  padding: 10px;
  text-align: center;
  margin-bottom: 20px;
  margin-left: 50px;
  margin-right: 50px;
  background-color: #e53935;
  color: #fff;
  border: 1px solid #c62828;
  border-radius: 5px;
`;

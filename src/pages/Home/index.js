import React from "react";
import { Link } from "react-router-dom";

import { Container } from "./styles";

const Home = () => {
  return (
    <Container>
      <h1>Home</h1>
      <Link to="">Home</Link>
    </Container>
  );
};

export default Home;

import React from "react";
import { Link } from "react-router-dom";

import { Container } from "./styles";

const NotFound = () => {
  return (
    <Container>
      <h1>NotFound</h1>
      <Link to="/">Home</Link>
    </Container>
  );
};

export default NotFound;

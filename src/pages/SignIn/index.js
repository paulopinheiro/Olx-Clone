import React, { useState } from "react";

import useApi from "@/helpers/OlxAPI";
import { doLogin } from "@/helpers/AuthHandler";

import { PageArea } from "./styles";
import {
  PageContainer,
  PageTitle,
  ErrorMessage
} from "@/styles/template/mainTemplate";

const SignIn = () => {
  const api = useApi();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [rememberPassword, setRememberPassword] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [error, setError] = useState("");

  const handleSubmit = async e => {
    e.preventDefault();
    setDisabled(true);

    const json = await api.login(email, password);

    if (json.error) {
      setError(json.error);
    } else {
      doLogin(json.token, rememberPassword);
      window.location.href = "/";
    }
  };

  return (
    <PageContainer>
      <PageTitle>Login</PageTitle>
      <PageArea>
        {error && <ErrorMessage>{error}</ErrorMessage>}

        <form onSubmit={handleSubmit}>
          <lable className="area">
            <div className="area-title">E-mail</div>
            <div className="area-input">
              <input
                id="input"
                type="email"
                disabled={disabled}
                value={email}
                onChange={e => setEmail(e.target.value)}
                required
              />
            </div>
          </lable>
          <lable className="area">
            <div className="area-title">Senha</div>
            <div className="area-input">
              <input
                id="input"
                type="password"
                disabled={disabled}
                value={password}
                onChange={e => setPassword(e.target.value)}
                required
              />
            </div>
          </lable>
          <lable className="area">
            <div className="area-title">Lembrar Senha</div>
            <div className="area-input">
              <input
                type="checkbox"
                disabled={disabled}
                checkbox={rememberPassword}
                onChange={() => setRememberPassword(!rememberPassword)}
              />
            </div>
          </lable>
          <lable className="area">
            <div className="area-title"></div>
            <div className="area-input">
              <button disabled={disabled}>Fazer Login</button>
            </div>
          </lable>
        </form>
      </PageArea>
    </PageContainer>
  );
};

export default SignIn;

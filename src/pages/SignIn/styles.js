import styled from "styled-components";

export const PageArea = styled.div`
  form {
    font-size: 16px;
    background-color: #fff;
    border-radius: 3px;
    padding: 20px;
    box-shadow: 0px 0px 3px #999;

    .area {
      display: flex;
      align-items: center;
      padding: 15px;
      max-width: 500px;

      .area-title {
        width: 200px;
        text-align: right;
        padding-right: 15px;
        font-weight: bold;
      }

      .area-input {
        flex: 1;

        #input {
          width: 100%;
          font-size: 16px;
          padding: 5px;
          border: 1px solid #b9b9b9;
          border-radius: 3px;
          transition: all ease 0.3s;

          &:focus {
            border: 1px solid #424242;
            color: #424242;
          }
        }
      }

      button {
        font-size: 16px;
        background-color: #1e88e5;
        border: 0px;
        padding: 10px 15px;
        border-radius: 4px;
        color: #fff;

        cursor: pointer;

        &:hover {
          background-color: #1565c0;
        }
      }
    }
  }
`;

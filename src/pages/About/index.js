import React from "react";
import { Link } from "react-router-dom";

import { Container } from "./styles";

const About = () => {
  return (
    <Container>
      <h1>About</h1>
      <Link to="/">Home</Link>
    </Container>
  );
};

export default About;
